import {
	http,
	Method
} from "@/utils/request.js";

export function getInfo(key) {
	return http.request({
		url: `/wsrcom/${key}`,
		method: Method.GET,
		needToken: false,
	});
}
